package com.qboxus.pimpcar.ActivitesFragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.database.DatabaseReference;
import com.ironsource.mediationsdk.IronSource;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.sdk.InterstitialListener;
import com.ironsource.mediationsdk.sdk.RewardedVideoListener;
import com.qboxus.pimpcar.ActivitesFragment.VideoRecording.VideoRecoderA;
import com.qboxus.pimpcar.Adapters.SearchAdapter;
import com.qboxus.pimpcar.Constants;
import com.qboxus.pimpcar.Models.SearchDataModel;
import com.qboxus.pimpcar.R;
import com.qboxus.pimpcar.SimpleClasses.Functions;
import com.qboxus.pimpcar.SimpleClasses.PermissionUtils;
import com.qboxus.pimpcar.SimpleClasses.Variables;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class ImgesActivity extends AppCompatActivity implements View.OnClickListener {

    Context context;
    RecyclerView recyclerView;
    ImageView btnBack, pimpModeButton,uploadImage;
    DatabaseReference rootref;
    String Imagetype = "";

    private String mCurrentPhotoPath;
    private static final int PICK_PHOTO = 12345;
    PermissionUtils takePermissionUtils;
    private static final int CAMERA_PIC_REQUEST = 54321;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Functions.setLocale(Functions.getSharedPreference(ImgesActivity.this).getString(Variables.APP_LANGUAGE_CODE, Variables.DEFAULT_LANGUAGE_CODE)
                , this, ImgesActivity.class, false);
        setContentView(R.layout.fragment_all_post);
        context = ImgesActivity.this;
        IronSource.init(this, "11e2d1a55", IronSource.AD_UNIT.BANNER);
        IronSource.init(this, "11e2d1a55", IronSource.AD_UNIT.INTERSTITIAL);
        if (!Constants.IS_REMOVE_ADS)
            loadAdd();
        IronSource.loadInterstitial();
// YOUR OTHER CODE //
// YOUR OTHER CODE //
// YOUR OTHER CODE //
        IronSource.setInterstitialListener(new InterstitialListener() {

            /**
             * Invoked when Interstitial Ad is ready to be shown after load function was called.
             */
            @Override
            public void onInterstitialAdReady() {

            }

            /**
             * invoked when there is no Interstitial Ad available after calling load function.
             */
            @Override
            public void onInterstitialAdLoadFailed(IronSourceError error) {

            }

            /**
             * Invoked when the Interstitial Ad Unit is opened
             */
            @Override
            public void onInterstitialAdOpened() {

            }

            /*
             * Invoked when the ad is closed and the user is about to return to the application.
             */
            @Override
            public void onInterstitialAdClosed() {
//                showAds("homeButton");

            }

            /**
             * Invoked when Interstitial ad failed to show.
             * @param error - An object which represents the reason of showInterstitial failure.
             */
            @Override
            public void onInterstitialAdShowFailed(IronSourceError error) {

            }

            /*
             * Invoked when the end user clicked on the interstitial ad, for supported networks only.
             */
            @Override
            public void onInterstitialAdClicked() {

            }

            /** Invoked right before the Interstitial screen is about to open.
             *  NOTE - This event is available only for some of the networks.
             *  You should NOT treat this event as an interstitial impression, but rather use InterstitialAdOpenedEvent
             */
            @Override
            public void onInterstitialAdShowSucceeded() {

            }
        });
        IronSource.setRewardedVideoListener(new RewardedVideoListener() {
            /**
             * Invoked when the RewardedVideo ad view has opened.
             * Your Activity will lose focus. Please avoid performing heavy
             * tasks till the video ad will be closed.
             */
            @Override
            public void onRewardedVideoAdOpened() {
//                Toast.makeText(ImgesActivity.this, "Rewarded ad opened", Toast.LENGTH_SHORT).show();

            }

            /*Invoked when the RewardedVideo ad view is about to be closed.
            Your activity will now regain its focus.*/
            @Override
            public void onRewardedVideoAdClosed() {
//                Toast.makeText(ImgesActivity.this, "ad closed", Toast.LENGTH_SHORT).show();
//                String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
//                        "/PimpCar";
//                File dir = new File(file_path);
//                if (!dir.exists())
//                    dir.mkdirs();
//                File file = new File(dir, "pimped_image" + System.currentTimeMillis() + ".png");
//                FileOutputStream fOut = null;
//                try {
//                    fOut = new FileOutputStream(file);
//                    main_bitmap_image.compress(Bitmap.CompressFormat.PNG, 85, fOut);
//                    fOut.flush();
//                    fOut.close();
//                    HelperMethods.showToastbar(PostEditActivity.this, "Image Saved Successfully");
//                } catch (FileNotFoundException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }

            }

            /**
             * Invoked when there is a change in the ad availability status.
             *
             * @param - available - value will change to true when rewarded videos are *available.
             *          You can then show the video by calling showRewardedVideo().
             *          Value will change to false when no videos are available.
             */
            @Override
            public void onRewardedVideoAvailabilityChanged(boolean available) {
                //Change the in-app 'Traffic Driver' state according to availability.
            }

            /**
             /**
             * Invoked when the user completed the video and should be rewarded.
             * If using server-to-server callbacks you may ignore this events and wait *for the callback from the ironSource server.
             *
             * @param - placement - the Placement the user completed a video from.
             */
            @Override
            public void onRewardedVideoAdRewarded(Placement placement) {

            }

            /* Invoked when RewardedVideo call to show a rewarded video has failed
             * IronSourceError contains the reason for the failure.
             */
            @Override
            public void onRewardedVideoAdShowFailed(IronSourceError error) {
            }

            /*Invoked when the end user clicked on the RewardedVideo ad
             */
            @Override
            public void onRewardedVideoAdClicked(Placement placement) {

            }

            @Override
            public void onRewardedVideoAdStarted() {
//                Toast.makeText(ImgesActivity.this, "Rewarded ad started", Toast.LENGTH_SHORT).show();
            }

            /* Invoked when the video ad finishes plating. */
            @Override
            public void onRewardedVideoAdEnded() {
            }
        });

//        takePermissionUtils = new PermissionUtils(ImgesActivity.this, mPermissionResult);
        btnBack = findViewById(R.id.back_btn);
        pimpModeButton = findViewById(R.id.pimpModeButton);
        uploadImage = findViewById(R.id.uploadImage);
        btnBack.setOnClickListener(this);
        uploadImage.setOnClickListener(this);
        pimpModeButton.setOnClickListener(this);
        recyclerView = (RecyclerView) findViewById(R.id.recylerview);
        setData();
    }

    @Override
    public void onDestroy() {
//        mPermissionResult.unregister();

        super.onDestroy();

    }

    private void setData() {

        SearchDataModel[] searchDataModels = new SearchDataModel[]{
                new SearchDataModel("perfect beautiful car pictures", R.drawable.demo1),
                new SearchDataModel("perfect beautiful car pictures", R.drawable.demo2),
                new SearchDataModel("perfect beautiful car pictures", R.drawable.demo3),
                new SearchDataModel("perfect beautiful car pictures", R.drawable.demo4),
                new SearchDataModel("perfect beautiful car pictures", R.drawable.demo1),
                new SearchDataModel("perfect beautiful car pictures", R.drawable.demo2),
                new SearchDataModel("perfect beautiful car pictures", R.drawable.demo3),
                new SearchDataModel("perfect beautiful car pictures", R.drawable.demo4),
                new SearchDataModel("perfect beautiful car pictures", R.drawable.demo1),
                new SearchDataModel("perfect beautiful car pictures", R.drawable.demo2),
                new SearchDataModel("perfect beautiful car pictures", R.drawable.demo3),
                new SearchDataModel("perfect beautiful car pictures", R.drawable.demo4),
                new SearchDataModel("perfect beautiful car pictures", R.drawable.demo1),
                new SearchDataModel("perfect beautiful car pictures", R.drawable.demo2),
                new SearchDataModel("perfect beautiful car pictures", R.drawable.demo3),
                new SearchDataModel("perfect beautiful car pictures", R.drawable.demo4),

        };

        SearchAdapter adapter = new SearchAdapter(searchDataModels);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(ImgesActivity.this));
        recyclerView.setAdapter(adapter);
    }
    // watch the streaming of user which will be live


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.back_btn:

                ImgesActivity.super.onBackPressed();
                break;

            case R.id.pimpModeButton:
                showAdd();
                IronSource.showRewardedVideo("DefaultRewardedVideo");
                showDialog(this);

                break;
            case R.id.uploadImage:
                showAdd();
                IronSource.showInterstitial("DefaultInterstitial");
                showDialog(this);

                break;
            default: {
            }
        }
    }

    public void showDialog(Activity activity) {

        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(activity);
        bottomSheetDialog.setContentView(R.layout.bottom_sheet_home);
        CircleImageView pimpmode = bottomSheetDialog.findViewById(R.id.circleImageView_pimpmode);
        CircleImageView upload = bottomSheetDialog.findViewById(R.id.circleImageView_upload);

        pimpmode.setOnClickListener(v -> {
            bottomSheetDialog.dismiss();
            showDialodPimpMode(activity, "pimpmode");
        });

        upload.setOnClickListener(v -> {
            bottomSheetDialog.dismiss();
            showDialodPimpMode(activity, "upload");
        });

        bottomSheetDialog.show();

    }

    private void showDialodPimpMode(Activity activity, String str) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setContentView(R.layout.select_pic_dialog_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        RelativeLayout gallery_button = dialog.findViewById(R.id.gallery_button);
        RelativeLayout camera_button = dialog.findViewById(R.id.camera_button);
        RelativeLayout video_button = dialog.findViewById(R.id.video_button);

        Imagetype = str;
        gallery_button.setOnClickListener(v -> {
            openGalleryWindow();
            dialog.dismiss();

        });

        camera_button.setOnClickListener(v -> {
            takePhotoFromCamera();
            dialog.dismiss();
        });

        video_button.setOnClickListener(v -> {
            dialog.dismiss();
            if (str.equals("pimpmode")) {
//            video_button.setVisibility(View.GONE);
//                openGalleryWindow();
                openGalleryForVideos();
            } else {
                openGalleryForVideos();

            }

        });
        dialog.show();
    }

    public void takePhotoFromCamera() {
        try {
            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (cameraIntent.resolveActivity(getPackageManager()) != null) {
                File photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException ex) {
                }
                if (photoFile != null) {

                    Uri photoURI = FileProvider.getUriForFile(this,
                            "com.pimpcar.provider",
                            photoFile);

                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);


                    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
                        cameraIntent.setClipData(ClipData.newRawUri("", photoURI));
                        cameraIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    }

                    startActivityForResult(cameraIntent, CAMERA_PIC_REQUEST);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private File createImageFile() throws IOException {
// Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";

        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName, // prefix
                ".jpg", // suffix
                storageDir // directory
        );

// Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void openGalleryWindow() {
        if (Build.VERSION.SDK_INT < 19) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.setType("image/* video/*");
            startActivityForResult(intent, PICK_PHOTO);
        } else {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_MIME_TYPES, new String[]{"image/*", "video/*"});
            startActivityForResult(intent, PICK_PHOTO);
        }
    }

    private void openGalleryForVideos() {
//        if (Build.VERSION.SDK_INT < 19) {
//            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//            intent.setType("video/*");
//            startActivityForResult(intent, PICK_PHOTO);
//        } else {
//            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
//            intent.addCategory(Intent.CATEGORY_OPENABLE);
//            intent.setType("video/*");
//            intent.putExtra(Intent.EXTRA_MIME_TYPES, new String[]{"video/*"});
//            startActivityForResult(intent, PICK_PHOTO);
//        }
        Intent intent = new Intent(this, VideoRecoderA.class);
        startActivity(intent);
        overridePendingTransition(R.anim.in_from_bottom, R.anim.out_to_top);
    }
    protected void onResume() {
        super.onResume();
        IronSource.onResume(this);
    }
    protected void onPause() {
        super.onPause();
        IronSource.onPause(this);
    }
    InterstitialAd mInterstitialAd;
    public void loadAdd() {

        mInterstitialAd = new InterstitialAd(context);
        mInterstitialAd.setAdUnitId(context.getResources().getString(R.string.my_Interstitial_Add));
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });


    }


    public void showAdd() {
        if (mInterstitialAd != null && mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        }
    }
}
