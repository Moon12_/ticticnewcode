package com.qboxus.pimpcar.ApiClasses.Interfaces;

public interface FragmentDataSend {

    void onDataSent(String yourData);
}
