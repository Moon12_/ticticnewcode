package com.qboxus.pimpcar.ApiClasses.Interfaces;

import android.view.View;

public interface AdapterClickListener {
    void onItemClick(View view, int pos, Object object);
}
