package com.qboxus.pimpcar.ApiClasses.Interfaces;

import android.content.pm.ResolveInfo;

public interface ShareIntentCallback {
    void onResponse(ResolveInfo resolveInfo);
}
