package com.qboxus.pimpcar.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.qboxus.pimpcar.Models.SearchDataModel;
import com.qboxus.pimpcar.R;


public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {
    private SearchDataModel[] listdata;

    // RecyclerView recyclerView;
    public SearchAdapter(SearchDataModel[] listdata) {
        this.listdata = listdata;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.profile_rv_single_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final SearchDataModel SearchDataModel = listdata[position];
        holder.post_description.setText(listdata[position].getDescription());
        holder.imageView.setImageResource(listdata[position].getImgId());

    }


    @Override
    public int getItemCount() {
        return listdata.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public AppCompatImageView imageView;
        public TextView textView;
        public AppCompatTextView post_description;

        public RelativeLayout relativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = itemView.findViewById(R.id.user_post_image);
            this.textView = (TextView) itemView.findViewById(R.id.user_name_tv);
            this.post_description = itemView.findViewById(R.id.post_description);
//            relativeLayout = (RelativeLayout)itemView.findViewById(R.id.rlTopp);
        }
    }
}